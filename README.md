# iKhokha Tech Check

### The Busy Shop
---
### Introduction

There is a busy fruit shop owner that requires a speedier way of checking-out orders. You are tasked with building the required native Android/iOS application. The app should target the latest stable version of the OS, supporting older versions is optional.

#### Prerequisites
  - Git
  - Applicable SDK
  - An IDE of your choice (Android Studio, Xcode, AppCode etc)

---
### Functionality
The application should include the following functionality:

 - The application should be able to scan an item barcode and add it to a shopping cart.
 - All data for the items should be pulled from a Firebase database (provided)
 - The shopping cart should indicate all items that have been scanned.
 - A user must be able to "check-out" their items from their shopping cart, this should result in an order summary.
 - If an order summary is confirmed, a receipt should be generated with the option to share it via Whatsapp, Gmail etc 
 - The receipt should reflect the date and time of the order, order summary, and total cost of order.

A Firebase database and storage is available for you to use:

Database:

 - https://the-busy-shop.firebaseio.com/
 - Contains all product items
 - See database.json in the repo for the data structure reference
 
Storage

 - gs://the-busy-shop.appspot.com
 - Contains all product images
 - Storage locations for images are provided in the database. Eg gs://the-busy-shop.appspot.com/apple.jpg as specified by the image field

The **google-services.json (Android) / GoogleService-Info.plist (iOS)** file is supplied and can be found in the root of this repo.

You can use the following credentials to authenticate with Firebase:
  
 - Email: **techcheck@ikhokha.com**
 - Password: **password**

##  
**PLEASE NOTE: These credentials will give you API access only and won't allow you to log in to the Firebase Console. All artifcacts
and information needed can be found within this repo**
##  

---
### Interface & Feature Set
You may use any design & interface you find appropriate and user friendly. They should include the following features:

 - Selecting an item from the shopping cart should display the item's image and any data associated with the item.
 - Scanning the same barcode twice should not duplicate items in the cart, but rather increase the quantity.
 - The user should be allowed to go back add more items to their shopping cart from their order summary.

---
### Guidelines
Barcodes are provided below (see annex) and are formatted in Code-128. Each barcode value corresponds to a product id as defined in the Firebase database.

You may make use of any API's and libraries you wish. The application should have good screen flows as well as an end-end user experience. We recommend that good material design principles and coding is applied throughout your applications. Once your application is complete and ready for submission, please generate an APK/IPA file, as well as .zip of your project source when submitting.

You may use one of the following languages:

 - Android: Java or Kotlin
 - iOS: Swift

Good Luck!

### Annex

![picture](https://bitbucket.org/ikhokha/the-busy-shop/raw/master/barcodes.png)
